#!/bin/bash

ASCIIDOC="asciidoctor"
WKHTMLTOPDF="wkhtmltopdf"
LIBREOFFICE="libreoffice"
DOC=( "Linux_game.adoc" "PIZEROW.adoc" )

for (( I = 0; I < ${#DOC[@]}; ++I ))
do
#  $ASCIIDOC --backend slidy "${DOC[$I]}"
#  echo "mv ${DOC[$I]/\.adoc/\.html} ${DOC[$I]/\.adoc/_slide\.html}"
#  mv "${DOC[$I]/\.adoc/\.html}" "${DOC[$I]/\.adoc/_slide\.html}"
  ${ASCIIDOC} --backend html5 "${DOC[$I]}"
#  ${LIBREOFFICE} --invisible --convert-to pdf "${DOC[$I]/\.adoc/\.pdf}" "${DOC[$I]/\.adoc/\.html}"
  $WKHTMLTOPDF -s A4 --disable-smart-shrinking "${DOC[$I]/\.adoc/\.html}" "${DOC[$I]/\.adoc/\.pdf}"
  $WKHTMLTOPDF -s B5 --disable-smart-shrinking "${DOC[$I]/\.adoc/\.html}" "${DOC[$I]/\.adoc/_B5\.pdf}"
done

